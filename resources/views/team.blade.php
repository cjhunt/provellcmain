@extends('Layouts.Layout2')

@section('content')

    <style>

        body {
            background-color: #DEB887;
        }

        /* Three columns side by side */
        .column {
            float: left;
            width: 33.3%;
            margin-bottom: 16px;
            padding: 0 8px;
        }

        /* Display the columns below each other instead of side by side on small screens */
        @media screen and (max-width: 650px) {
            .column {
                width: 100%;
                display: block;
            }
        }

        /* Add some shadows to create a card effect */
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }

        h1, h2 {
            font-family: 'Frank Ruhl Libre', serif;
        }


        /* Some left and right padding inside the container */
        .container {
            padding: 0 16px;
        }

        /* Clear floats */
        .container::after, .row::after {
            content: "";
            clear: both;
            display: table;
        }

        .title {
            color: grey;
        }

        .myButton {
            -moz-box-shadow: 0px 1px 0px 0px #f0f7fa;
            -webkit-box-shadow: 0px 1px 0px 0px #f0f7fa;
            box-shadow: 0px 1px 0px 0px #f0f7fa;
            background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #81cde6), color-stop(1, #4dc1eb));
            background:-moz-linear-gradient(top, #81cde6 5%, #4dc1eb 100%);
            background:-webkit-linear-gradient(top, #81cde6 5%, #4dc1eb 100%);
            background:-o-linear-gradient(top, #81cde6 5%, #4dc1eb 100%);
            background:-ms-linear-gradient(top, #81cde6 5%, #4dc1eb 100%);
            background:linear-gradient(to bottom, #81cde6 5%, #4dc1eb 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#81cde6', endColorstr='#4dc1eb',GradientType=0);
            background-color:#81cde6;
            -moz-border-radius:6px;
            -webkit-border-radius:6px;
            border-radius:6px;
            border:1px solid #39a8ed;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:Arial;
            font-size:16px;
            padding:6px 24px;
            text-decoration:none;
            text-shadow:0px -1px 0px #5b6178;
        }
        .myButton:hover {
            background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #4dc1eb), color-stop(1, #81cde6));
            background:-moz-linear-gradient(top, #4dc1eb 5%, #81cde6 100%);
            background:-webkit-linear-gradient(top, #4dc1eb 5%, #81cde6 100%);
            background:-o-linear-gradient(top, #4dc1eb 5%, #81cde6 100%);
            background:-ms-linear-gradient(top, #4dc1eb 5%, #81cde6 100%);
            background:linear-gradient(to bottom, #4dc1eb 5%, #81cde6 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#4dc1eb', endColorstr='#81cde6',GradientType=0);
            background-color:#4dc1eb;
        }
        .myButton:active {
            position:relative;
            top:1px;
        }


        /* Style the accordion panel. Note: hidden by default */
        .panel {
            padding: 0 18px;
            background-color: white;
            display: none;
            overflow: hidden;
        }
    </style>

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner card_mine">
            <div class="carousel-item active card_mine">
                <img class="d-block w-100" src="/images/map2.jpg" alt="First slide">
                <div class="carousel-caption">
                    <h1>PROVE Team Information</h1>
                </div>
            </div>
        </div>
    </div>



<div class="container mt-5">
    <div class="row">
        <div class="column">
            <div class="card">
                <img src="/images/drburt.jpg" alt="John" style="width:100%">
                <div class="container">
                    <center><h2>Dr. John Burt</h2></center>
                    <center><p class="title">John W. Burt PhD</p></center>
                    <button class="myButton accordion mb-2 mx-auto d-block">Biography</button>
                    <div class="panel">
                        <p> Dr. Burt has a Doctorate in Clinical Psychology and a Master's in Creative Arts Therapy from <a href="https://www.hahnemannhospital.com/SitePages/Home.aspx">Hahnemann
                                University</a> in Philadelphia, in addition to a Bachelor's in Music Therapy from <a href="https://www.csulb.edu/">California State
                                University in Long Beach</a>. He is a Licensed Psychologist in the State of <a href="https://www.colorado.gov/dora">Colorado Department of
                                Regulatory Agencies</a>, approved for independent practice since 1996. He has been Clinical Director at
                            PROVE and an Approved Provider for Domestic Violence Treatment through the <a href="https://www.colorado.gov/pacific/dcj/domestic-violence-offender-management">Colorado Domestic
                                Violence Offender Management Board</a> since 2009. He has seen hundreds of clients through the
                            domestic violence treatment process and has performed thousands of mental health evaluations. He taught
                            psychology at <a href="https://www.frontrange.edu/">Front Range Community College</a> for 13 years and was Clinical Director of Adolescent
                            Treatment Services at <a href="https://www.centura.org/locations/longmont-united-hospital">Longmont United Hospital</a> for 3 years. He completed his Internship and was
                            employed at the <a href="https://www.connecticut.va.gov/">National Center for PTSD at the West Haven CT VA Hospital</a> for 3 years. Prior to
                            that he had 4 years of work as a Music Therapist at inpatient psychiatric hospitals in Southern Califonia. He
                            is originally from Wilmington, Delaware and has been a resident of Northern Colorado since 1995. In his
                            free time, he enjoys jogging, hiking 14ers, playing music, and spending time with his wife and dogs at their
                            historic home.
                            <br>
                            <br>
                            He can be reached at PROVE at 970-482-4334 or at <a href="mailto:provecounseling@gmail.com">provecounseling@gmail.com</a>.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="column">
            <div class="card">
                <img src="/images/nealc.jpg" alt="Neal" style="width:100%">
                <div class="container">
                    <center><h2>Dr. Neal Chase</h2></center>
                        <center><p class="title">Neal Chase PhD</p></center>
                    <button class="myButton accordion mb-2 mx-auto d-block">Biography</button>
                    <div class="panel">
                        <p> Dr Chase has a Doctorate in Psychology at <a href="https://www.saybrook.edu/">Saybrook University</a>. He holds a Master's of Science in
                            Psychology from <a href="https://www.capella.edu/">Capella University</a> and a Bachelor of the Arts in Psychology from the <a href="https://www.umt.edu/">University of Montana</a>
                            . He is a Registered Psychotherapist through the <a href="https://www.colorado.gov/dora">Colorado Department of Regulatory Agencies</a>.
                            At PROVE, he co-leads educational and group therapy sessions as well as providing individual counseling
                            services. He is an Accredited <a href="https://www.caringdads.org/">Caring Dads</a> Professional/Facilitator, trained to teach parenting groups;
                            and is in training for Domestic Violence offender treatment. He maintained a private practice at Positive Counseling on the Western Slope
                            of Colorado prior to moving to Fort Collins, and has experience in group therapy at the Montana State Prison
                            System.
                            <br>
                            <br>
                            He can be reached at PROVE at 970-482-4334 or at <a href="mailto:provecounseling@gmail.com">provecounseling@gmail.com</a>.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="column">
            <div class="card">
                <img src="/images/S.jpg" alt="Advocate" style="width:100%">
                <div class="container">
                    <center><h2>S</h2></center>
                    <center><p class="title">Treatment Victim Advocate</p></center>
                    <button class="myButton accordion mb-2 mx-auto d-block">Biography</button>
                    <div class="panel">
                        <p> Our Treatment Victim Advocate holds a Master's in Community Counseling from <a href="https://www.regis.edu/">Regis University</a>
                            and a Bachelors in Communication from <a href="https://www.bc.edu/">Boston College</a>. She is a Licensed Professional Counselor
                            through the <a href="https://www.colorado.gov/dora">Colorado Department of Regulatory Agencies</a> and is a Certified Treatment Victim
                            Advocate through the <a href="https://www.colorado.gov/pacific/dcj/domestic-violence-offender-management">Colorado Domestic Violence Offender Management Board</a>. She has 6 years
                            experience at PROVE and provides victim services including crisis management and referral services to the
                            identified victims of PROVE clients. She maintains a position as a therapist at an area community mental
                            health agency.
                            <br>
                            <br>
                            She can be reached at PROVE at 970-482-4334 or at <a href="mailto:provevictimadvocate@gmail.com">provevictimadvocate@gmail.com</a>.</p>
                        <p><i><b>Identified offenders are prohibited from contacting her.</b></i></p>
                    </div>
                </div>
            </div>
        </div>


                {{--<div class="column">
                    <div class="card">
                        <img src="/images/jess.jpg" alt="Intern" style="width:100%">
                        <div class="container">
                            <center><h2>J</h2></center>
                            <center><p class="title">Intern</p></center>
                            <button class="myButton accordion mb-2 mx-auto d-block">Biography</button>
                            <div class="panel">
                                <p> Our Intern has a Masters of Education and Clinical Health Counseling from <a href="https://www.lamar.edu/">Lamar University</a> in Texas. She is a
                                    Licensed Professional Counselor Candidate, and a National Certified Counselor.
                                    <br>
                                    <br>
                                    She can be reached at PROVE at 970-482-4334 or at <a href="mailto:menspsychhealth@gmail.com">menspsychhealth@gmail.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
--}}

        <div class="container mb-5">
            <div class="row">



        <div class="column">
            <div class="card">
                <img src="/images/e.jpg" alt="AdministrativeAssistant" style="width:100%">
                <div class="container">
                    <center><h2>Ellie</h2></center>
                        <center><p class="title">Administrative Assistant</p></center>
                    <button class="myButton accordion mb-2 mx-auto d-block">Biography</button>
                    <div class="panel">
                        <p> Our Administrative Assistant is currently a 4th
                            year student at <a href="https://www.colostate.edu/">Colorado State University</a> pursuing a
                            Major in Human Development in Family. She greets and signs in clients for classes, answers
                            phones, returns messages and emails, schedules clients for appointments, scores tests, and prepares charts
                            and records.
                            <br>
                            <br>
                            She can be reached at PROVE at 970-482-4334 or at <a href="mailto:provecounseling@gmail.com">provecounseling@gmail.com</a>.</p>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
</div>
</center>

    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                /* Toggle between adding and removing the "active" class,
                to highlight the button that controls the panel */
                this.classList.toggle("active");

                /* Toggle between hiding and showing the active panel */
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>

@endsection