@component('mail::message')
# Buying Bulk Product

Inquiry about buying in bulk,

@component('mail::panel')
{{--Name of Company--}}
Company Name: Dollar Tree
@endcomponent

{{--Message--}}
I would like to purchase blah. Blah blah. I would like to purchase blah. Blah blah. I would like to purchase blah. Blah blah. I would like to purchase blah. Blah blah.
I would like to purchase blah. Blah blah. I would like to purchase blah. Blah blah. I would like to purchase blah. Blah blah.I would like to purchase blah. Blah blah.

@component('mail::table')
    |Country| State| Contact Info  |
    | ------------- |:-------------:| --------:|
    |United States|Colorado|cjhunt26@gmail.com|
    |             |        |      970-459-7144|
@endcomponent





Thanks,<br>
{{ config('app.name') }}
@endcomponent
