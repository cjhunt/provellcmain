@extends('Layouts.Layout')

@section('content')

    <style>

        h6, h3, h2, h4 {
            font-family: 'Frank Ruhl Libre', serif;
        }

        h1, h2 {
            font-family: 'Frank Ruhl Libre', serif;
        }

        #leftSep {
            width: 100vw;
            margin-left: -50vw;
            left: 50%;
            background-color: #e6f2ff;
        }

        #rightSep {
            width: 100vw;
            margin-right: -50vw;
            right: 50%;
            background-color: #e6f2ff;
        }

        #leftBrown {
            width: 100vw;
            margin-left: -50vw;
            left: 50%;
            background-color: #DEB887;
        }

        #rightBrown {
            width: 100vw;
            margin-right: -50vw;
            right: 50%;
            background-color: #DEB887;
        }


    </style>

    <center>

        <div class="container">

            <div id="leftBrown">
                <div id="rightBrown" style="padding-bottom: 50px; padding-top: 50px;">
                    <ul style="list-style: none;" class="mt-2">
                        <li><a href="#mwf_part"><b><h3>Men with Female Partners</b></h2></a></li>
                        <li class="mt-2"><a href="#wwm_part"><h3><b>Women with Male Partners</b></h3></a></li>
                        <li class="mt-2"><a href="#mwm_part"><h3><b>Men with Male Partners</b></h3></a></li>
                        <li class="mt-2"><a href="#wwf_part"><h3><b>Women with Female Partners</b></h3></a></li>
                    </ul>

                    <h4 class="mt-5"><b>Call or Email to schedule an appointment</b></h4>
                    <i class="fa fa-envelope fa-md mr-2"></i><a href="mailto:provecounseling@gmail.com">provecounseling@gmail.com</a>
                    <br>
                    <i class="fa fa-phone fa-md mr-2"></i>(Office) 970-482-4334

                </div>
            </div>

    <div id="leftSep">
        <div id="rightSep">
            <div class="container" id="mwf_part" style="margin-top: -16px; padding-top: 50px; padding-bottom: 50px;">
                <div class="container">
                    <h2><b>Men with Female Partners</b></h2>

                    <h4 class="mt-5">Weekly Education Classes(Phase 1)</h4>
                    <li>Monday 5:30 PM</li>
                    <li>Tuesday 11:30 AM</li>
                    <li>Tuesday 7:00 PM</li>


                    <h4 class="mt-5">Lower Risk(Phase 2)</h4>
                    <li>Tuesday 5:30 PM</li>
                    <h4 class="mt-5">Higher Risk(Phase 2)</h4>
                    <li>Thursday 5:30 PM</li>

                    <h4 class="mt-5">Monthly Individual Session</h4>
                    <li>Individually Scheduled</li>
                </div>
            </div>
        </div>
    </div>

    <div id="leftBrown">
        <div id="rightBrown">
            <a id="wwm_part">
                <div class="container" style="margin-bottom: -16px; padding-top: 50px; padding-bottom: 50px;">
                    <h2><b>Women with Male Partners</b></h2>
                    <h4 class="mt-5">Weekly Education Class or Group Therapy</h4>
                    <li>Wednesday 5:30 PM</li>

                    <h4 class="mt-5">Monthly Individual Session</h4>
                    <li>Individually Scheduled</li>
                </div>
            </a>
        </div>
    </div>
    </div>

    <div id="leftSep">
        <div id="rightSep">
            <div class="container" id="mwm_part" style="margin-top: -16px; padding-top: 50px; padding-bottom: 50px;">
                <div class="container">
                    <h2><b>Men with Male Partners</b></h2>

                    <h4 class="mt-5">Weekly Education Class or Group Therapy</h4>
                    <li>Call for Schedule - Subject to Change</li>

                    <h4 class="mt-5">Monthly Individual Session</h4>
                    <li>Individually Scheduled</li>
                </div>
            </div>
        </div>
    </div>

    <div id="leftBrown">
        <div id="rightBrown">
            <a id="wwf_part">
                <div class="container" style="margin-bottom: -16px; padding-top: 50px; padding-bottom: 50px;">
                    <h2><b>Women with Female Partners</b></h2>
                    <h4 class="mt-5">Weekly Education Class or Group Therapy</h4>
                    <li>Call for Schedule - Subject to Change</li>

                    <h4 class="mt-5">Monthly Individual Session</h4>
                    <li>Individually Scheduled</li>
                </div>
            </a>
        </div>
    </div>
    </div>


    </center>

@endsection