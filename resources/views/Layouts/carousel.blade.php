

<style>

    h1, h2 {
        font-family: 'Frank Ruhl Libre', serif;
    }
</style>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval=5000 data-pause=false>
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner card_mine">
        <div class="carousel-item active">
            <img class="d-block w-100" src="/images/ship.jpg" alt="First slide">
            <div class="carousel-caption">
                <h1>PEACEFUL RESOLUTIONS OF VOLATILE ENCOUNTERS</h1>
            </div>
        </div>
        <div class="carousel-item card_mine">
            <img class="d-block w-100" src="/images/map.jpg" alt="Second slide">
                <div class="carousel-caption">
                    <h1>DOMESTIC VIOLENCE OFFENDER TREATMENT</h1>
                </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>