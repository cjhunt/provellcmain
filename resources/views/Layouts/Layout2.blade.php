<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>

<style>

    .bg_myblue {
        background-color: #80bdff;
    }

</style>

<nav class="navbar navbar-expand-md navbar-dark bg_myblue">
    <a href="/" class="navbar-brand" style="color: white;">PROVE LLC</a>
    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#collapse_target">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="collapse_target">
        <ul class="navbar-nav mr-auto ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="/" style="color: white;">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/team" style="color: white;">Our Team</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/services" style="color: white;">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/contact" style="color: white;">Contact and Location</a>
            </li>
        </ul>
    </div>
</nav>

<div style="min-height: calc(100vh - 216px)">
@yield('content')
</div>

<center><footer class="section footer-classic text-white bg_myblue align-bottom" style="padding-top: 15px;">
        <div class="container">
            <div class="row">


                <div class="col-xs-12 col-md-6">
                    <h5><b>Contact Information</b></h5>
                    <i class="fa fa-map-marker fa-md mr-2"></i>1217 East Elizabeth Street, Suite 6A, in Fort Collins, CO
                    <br>
                    <i class="fa fa-envelope fa-md mr-2"></i><a href="mailto:provecounseling@gmail.com" style="color: #8a705d;">provecounseling@gmail.com</a>
                    <br>
                    <i class="fa fa-phone fa-md mr-2"></i>(Office) 970-482-4334

                    <br>
                    <i class="fa fa-fax fa-md mr-2"></i>(Fax) 970-407-1339

                </div>
                <div class="col-md-6 col-xs-12">
                    <h5><b>Links</b></h5>
                    <a href="/services" style="color: #8a705d;">Services</a>
                    <br>
                    <a href="/servicetimes" style="color: #8a705d;">Service Schedule</a>
                    <br>
                    <a href="/team" style="color: #8a705d;">Our Team</a>
                    <br>
                    <a href="/contact" style="color: #8a705d;">Contact and Location</a>
                </div>
            </div>
        </div>
        <br>
        <center> <i class="fa fa-copyright fa-md mr-2" style="padding-bottom: 5px;"></i>2019 PROVE LLC, All rights reserved.</center>
    </footer>
</center>

<script src="{{ mix('js/app.js') }}"></script>
@yield('js')
</body>
</head>
</html>

