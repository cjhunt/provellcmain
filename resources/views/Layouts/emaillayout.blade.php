<style>
    .field_required {
        color: #FF0000;
        font-weight: bold;
        vertical-align: top;
    }
</style>

<div class="container">

    <p style="width: 100px; height: 40px; margin-top: 40px;"><center><b>BECOME A DISTRIBUTOR</b></center></p>

    <p style="margin-top: 30px;">DASCO's sales professionals get to know our customers and stay informed of the most recent changes in cost, supply and demand, enabling you to get the highest quality product at the best price. We have a sales team member devoted to your region, which allows us to stay on top of your local trends, and alert you when we see a product you use at a price you might like. We understand the logistics behind getting you the products you need, cost-effectively and efficiently.</p>


    <p style="margin-top: 20px; margin-bottom: 50px;">Join the expanding DEF industry as a distibutor for DASCO and benefit from our growing customer base. DASCO is continually adding new suppliers, and meeting the demand for DEF products necessary for meeting emissions regulations implemented around the world.</p>

</div>

<div class="container">

    <form method="post" name="frmContact" action="http://www.dascoinc.com/diesel_exhaust_fluid?form=sendContactEmail">
        <table class="table-striped" width="500" cellpadding="0" cellspacing="0" border="0">
            <tbody><tr>
                <td width="10">&nbsp;</td>
                <td colspan="4" class="field_required">* indicates a Required field</td>
            </tr>
            <tr>
                <td width="10">&nbsp;</td>
                <td width="10" class="field_required">*</td>
                <td class="field_label">Company Name:</td>
                <td width="10">&nbsp;</td>
                <td align="left" valign="top">
                    <input type="text" id="inputCompanyName" name="inputCompanyName" size="23" class="field_input" value="">
                </td></tr>
            <tr>
                <td width="10">&nbsp;</td>
                <td width="10" class="field_required">*</td>
                <td class="field_label">Contact Name:</td>
                <td width="10">&nbsp;</td>
                <td align="left" valign="top">
                    <input type="text" id="inputContactName" name="inputContactName" size="23" class="field_input" value="">
                </td></tr>
            <tr>
                <td width="10">&nbsp;</td>
                <td width="10" class="field_required">*</td>
                <td class="field_label">Contact Email:</td>
                <td width="10">&nbsp;</td>
                <td align="left" valign="top">
                    <input type="text" id="inputContactEmail" name="inputContactEmail" size="23" class="field_input" value="">
                </td></tr>
            <tr>
                <td width="10">&nbsp;</td>
                <td width="10" class="field_required">*</td>
                <td class="field_label">Phone Number:</td>
                <td width="10">&nbsp;</td>
                <td align="left" valign="top">
                    <input type="text" id="inputPhone" name="inputPhone" size="23" class="field_input" value="">
                </td></tr>
            <tr>
                <td width="10">&nbsp;</td>
                <td width="10" class="field_required">*</td>
                <td class="field_label">City:</td>
                <td width="10">&nbsp;</td>
                <td align="left" valign="top">
                    <input type="text" id="inputCity" name="inputCity" size="23" class="field_input" value="">
                </td></tr>
            <tr>
                <td width="10">&nbsp;</td>
                <td width="10" class="field_required">*</td>
                <td class="field_label">State/Province:<br>(Required if <br>US, Canada, Mexico only)<br><br></td>
                <td width="10">&nbsp;</td>
                <td align="left" valign="top">
                    <select id="inputStateProvince" name="inputStateProvince" class="field_input"><option value=""></option>
                        <option value="" disabled="">----------  United States  ----------</option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska </option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa   </option>
                        <option value="KS">Kansas </option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine  </option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada </option>
                        <option value="NH">New Hampshire </option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina </option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio   </option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon </option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina </option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                        <option value="" disabled="">--------------  Canada  --------------</option>
                        <option value="AB">Alberta</option>
                        <option value="BC">British Columbia</option>
                        <option value="MB">Manitoba</option>
                        <option value="NB">New Brunswick</option>
                        <option value="NL">Newfoundland and Labrador</option>
                        <option value="NT">Northwest Territories</option>
                        <option value="NS">Nova Scotia</option>
                        <option value="NU">Nunavut</option>
                        <option value="ON">Ontario</option>
                        <option value="PE">Prince Edward Island</option>
                        <option value="QC">Quebec</option>
                        <option value="SK">Saskatchewan</option>
                        <option value="YT">Yukon</option>
                        <option value="" disabled="">--------------  Mexico  --------------</option>
                        <option value="AG">Aguascalientes</option>
                        <option value="BN">Baja California</option>
                        <option value="BS">Baja California Sur</option>
                        <option value="CM">Campeche</option>
                        <option value="CP">Chiapas</option>
                        <option value="CH">Chihuahua</option>
                        <option value="CA">Coahuila</option>
                        <option value="CL">Colima</option>
                        <option value="DF">Federal District</option>
                        <option value="DU">Durango</option>
                        <option value="GT">Guanajuato</option>
                        <option value="GR">Guerrero</option>
                        <option value="HI">Hidalgo</option>
                        <option value="JA">Jalisco</option>
                        <option value="MX">Mexico</option>
                        <option value="MC">Michoacan</option>
                        <option value="MR">Morelos</option>
                        <option value="NA">Nayarit</option>
                        <option value="NL">Nuevo Leon</option>
                        <option value="OA">Oaxaca</option>
                        <option value="PU">Puebla</option>
                        <option value="QE">Queretaro</option>
                        <option value="QR">Quintana Roo</option>
                        <option value="SL">San Luis Potosi</option>
                        <option value="SI">Sinaloa</option>
                        <option value="SO">Sonora</option>
                        <option value="TB">Tabasco</option>
                        <option value="TM">Tamaulipas</option>
                        <option value="TL">Tlaxcala</option>
                        <option value="VE">Veracruz</option>
                        <option value="YU">Yucatan</option>
                        <option value="ZA">Zacatecas</option>
                    </select>
                </td></tr>
            <tr>
                <td width="10">&nbsp;</td>
                <td width="10" class="field_required">*</td>
                <td class="field_label">Country:</td>
                <td width="10">&nbsp;</td>
                <td align="left" valign="top">
                    <select id="inputCountry" name="inputCountry" class="field_input"><option value=""></option>
                        <option value="" disabled="">----------  North America  ---------</option>
                        <option value="US">United States</option>
                        <option value="Canada">Canada</option>
                        <option value="Mexico">Mexico</option>
                        <option value="" disabled="">---  Australia / New Zealand  --</option>
                        <option value="Australia">Australia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="" disabled="">----------------  Asia -----------------</option>
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Brunei">Brunei</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="China">China</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran">Iran</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Israel">Israel</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Laos">Laos</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Nepal">Nepal</option>
                        <option value="North Korea">North Korea</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palestine">Palestine</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Singapore">Singapore</option>
                        <option value="South Korea">South Korea</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="Syria">Syria</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Timor-Leste">Timor-Leste</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="United Arab Emirate">United Arab Emirates</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Yemen">Yemen</option>
                    </select>
                </td></tr>
            <tr>
                <td width="10">&nbsp;</td>
                <td width="10" &nbsp;=""> </td>
                <td class="field_label">Comments:</td>
                <td width="10">&nbsp;</td>
                <td align="left" valign="top">
                    <textarea id="inputComments" name="inputComments" class="field_input" cols="40" rows="5"></textarea>
                </td></tr>
            <tr>
                <td colspan="4">&nbsp;</td>
                <td align="left" valign="top"><br>
                    <table cellspacing="0" cellpadding="3" border="1">
                        <tbody><tr>
                            <td align="center" style="height: 40px; background: url(images/captcha_background.jpg); padding-bottom: 0px; 0px 0px 0px;">Security Code<br><img src="images/825743.png" border="0"><img src="images/355212.png" border="0"><img src="images/224542.png" border="0"><img src="images/152638.png" border="0"><img src="images/240038.png" border="0"><img src="images/152638.png" border="0">     </td>
                        </tr>
                        <tr>
                            <td align="center" style="padding-bottom: 5px;">
                                <span style="font-size: 10pt;">Type the Security Code</span><br>
                                <input type="text" id="inputCaptchaNumber" name="inputCaptchaNumber" value="" size="10">
                                <input type="hidden" id="inputCaptchaKey" name="inputCaptchaKey" value="3196">
                            </td>
                        </tr>
                        </tbody></table>

                </td>
            </tr>
            </tbody></table>
        <input type="hidden" name="inputRequestType" value="becomeADistributor">
        <input type="submit" name="submit" value="Send Email" style="margin: 10px 0px 20px 180px;" onclick="return validateForm();">
    </form>
</div>

<script>
    $(document).ready(function(){
        console.log(country.info('US'))
    })
</script>