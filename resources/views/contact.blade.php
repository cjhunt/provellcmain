@extends('Layouts.Layout')

@section('content')


    <style>

        h6, h3, h2 {
            font-family: 'Frank Ruhl Libre', serif;
            color: black
        }

        h1, h2 {
            font-family: 'Frank Ruhl Libre', serif;
        }

        body {
            background-color: #DEB887;
        }

        /* Three image containers (use 25% for four, and 50% for two, etc) */
        .column {
            float: left;
            width: 50%;
            padding: 5px;
        }

        /* Clear floats after image containers */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }



    </style>

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner card_mine">
            <div class="carousel-item active card_mine">
                <img class="d-block w-100" src="/images/building.jpg" alt="First slide">
                <div class="carousel-caption">
                    <h1>1217 East Elizabeth Street, Suite 6A, Fort Collins, CO</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="padding-bottom: 50px;">
        <h3 style="margin-top: 50px;"><center><b>Located at the South West Corner of Luke and Elizabeth</b></center></h3>
        <h3 style="margin-top: 50px;"><center><b>Red brick building, #6, down the stairs</b></center></h3>

        <center><div class="col-xs-12 col-md-6 mt-4">
            <h5><b>Contact Information</b></h5>
            <i class="fa fa-map-marker fa-md mr-2"></i>1217 East Elizabeth Street, Suite 6A, in Fort Collins, CO
            <br>
            <i class="fa fa-envelope fa-md mr-2"></i><a href="mailto:provecounseling@gmail.com">provecounseling@gmail.com</a>
            <br>
            <i class="fa fa-phone fa-md mr-2"></i>(Office) 970-482-4334

            <br>
            <i class="fa fa-fax fa-md mr-2"></i>(Fax) 970-407-1339
        </div></center>

        <div class="row">
            <div class="column">
                <iframe style="margin-top:60px; margin-bottom: 50px; border: 1px solid #000; width: 100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3030.529560854651!2d-105.05698159687863!3d40.574064920257506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDDCsDM0JzI3LjEiTiAxMDXCsDAzJzIxLjkiVw!5e0!3m2!1sen!2sus!4v1560978353756!5m2!1sen!2sus" width="401" height="401" frameborder="0" border="0" allowfullscreen></iframe>
            </div>
            <div class="column">
                <img style="margin-top:60px; margin-bottom: 50px; border: 1px solid #000; width: 400px; height: 400px; width: 100%;" src="/images/building2.jpg" alt="Forest">
            </div>
        </div>

        {{--<iframe class="d-block" style="margin-top:60px; margin-bottom: 50px; border: 1px solid #000;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3030.529560854651!2d-105.05698159687863!3d40.574064920257506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDDCsDM0JzI3LjEiTiAxMDXCsDAzJzIxLjkiVw!5e0!3m2!1sen!2sus!4v1560978353756!5m2!1sen!2sus" width="400" height="400" frameborder="0" border="0" allowfullscreen></iframe>
        <img class="d-block" src="/images/building2.jpg">--}}

    </div>

@endsection