@extends('Layouts.Layout')

@section('content')

    @include('Layouts.carousel')

    <style>

        h6, h3, h2 {
            font-family: 'Frank Ruhl Libre', serif;
        }

        body {
            background-color: #DEB887;
        }

    </style>

    <div class="container" style="padding-bottom: 50px;">

        <center><h1 style="margin-top: 30px; padding-top: 50px;"><b>PROVE COUNSELING</b></h1></center>
        <center><h3 style="margin-top: 10px; padding-top: 40px;"><b><a href="/servicetimes">Domestic Violence Offender Treatment</a></b></h3></center>
        <center><h3 style="margin-top: 10px; padding-top: 20px;"><b>Anger Management</b></h3></center>
        <center><h3 style="margin-top: 10px; padding-top: 20px;"><b>Other Court Ordered Treatment</b></h3></center>

        <h3 style="margin-top: 100px;"><center><u><b>Our Mission</b></u></center></h3>

        <h6 style="margin-top: 50px; padding-bottom: 50px;"><center>PROVE LLC has been a local leader in domestic violence offender treatment for
            over 30 years. Our mission is to provide services for offenders and victims of domestic violence to reduce
            violence and abuse in homes in the Northern Colorado area. We provide educational classes and therapeutic
            group therapy to court-ordered and voluntary clients who seek to reduce their anger, abuse, and violence.</center></h6>

    </div>

@endsection


