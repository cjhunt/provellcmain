@extends('Layouts.Layout')

@section('content')


    <style>

        h6, h3, h2, h4 {
            font-family: 'Frank Ruhl Libre', serif;
        }

        h1, h2 {
            font-family: 'Frank Ruhl Libre', serif;
        }

        #leftSep {
            width: 100vw;
            margin-left: -50vw;
            left: 50%;
            background-color: #e6f2ff;
        }

        #rightSep {
            width: 100vw;
            margin-right: -50vw;
            right: 50%;
            background-color: #e6f2ff;
        }

        #leftBrown {
            width: 100vw;
            margin-left: -50vw;
            left: 50%;
            background-color: #DEB887;
        }

        #rightBrown {
            width: 100vw;
            margin-right: -50vw;
            right: 50%;
            background-color: #DEB887;
        }


    </style>

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner card_mine">
            <div class="carousel-item active card_mine">
                <img class="d-block w-100" src="/images/ship3.jpg" alt="First slide">
                <div class="carousel-caption">
                    <h1>PROVE PROVIDES THE FOLLOWING SERVICES</h1>
                </div>
            </div>
        </div>
    </div>

    <center>
    <center class="container">

        <div id="leftBrown">
            <div id="rightBrown" style="padding-bottom: 50px; padding-top: 50px;">
                <h4><b><a href="/servicetimes">Domestic Violence Offender Treatment</a></b></h4>
                <h4>for court-ordered clients</h4>
                <h4 style="padding-top: 20px;"><b>Anger Management</b></h4>
                <h4>for voluntary or court-ordered clients</h4>
                <h4>scheduled on an as-needed basis</h4>
                <h4 style="padding-top: 20px;"><b>Voluntary Domestic Violence Education</b></h4>
                <h4>scheduled on an as-needed basis</h4>
                <h4 style="padding-top: 20px;"><b>Caring Dads Parenting Class</b></h4>
                <h4>for voluntary or court-ordered clients</h4>
                <h4>scheduled on an as-needed basis</h4>
                <h4>Contact <a href="mailto:provecounseling@gmail.com">Neal Chase</a></h4>
                <h4 style="padding-top: 20px;"><b>Follow-up individual counseling for Graduates</b></h4>
                <h4 style="padding-top: 20px;"><b>Follow-up couples counseling for Graduates</b></h4>
            </div>
        </div>
    </center>

    </center>

@endsection